#ifndef _UT_GF16_H_
#define _UT_GF16_H_



#ifdef  __cplusplus
extern  "C" {
#endif


unsigned char ut_gf16_is_nonzero( unsigned char a );

unsigned char ut_gf16_mul( unsigned char a , unsigned char b );

unsigned char ut_gf16_squ( unsigned char a );

unsigned char ut_gf16_inv( unsigned char a );

unsigned char ut_gf16_mul_4( unsigned char a );

unsigned char ut_gf16_mul_8( unsigned char a );


#ifdef  __cplusplus
}
#endif

#endif
