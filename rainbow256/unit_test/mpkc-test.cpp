
#include <stdio.h>

#include "rainbow_config.h"

#include "rainbow.h"

#include "mpkc.h"

#include "prng_utils.h"


#define TEST_RUN 100

const unsigned qterms = TERMS_QUAD_POLY(_PUB_N);


int main()
{

	prng_seed_file("/dev/random");

        printf("%s\n", _S_NAME );

        printf("MQ key size: %d\n", _PUB_KEY_LEN );
	printf("MQ(N,M): %d,%d\n", _PUB_N , _PUB_M );

	printf("\n\n");
	printf("  Tester for the consisitency of following 2 functions.\n\n");
	printf("  mpkc_pub_map_gf256()      :   Evaluating MQ in public map\n" );
	printf("  mpkc_pub_map_gf256_n_m()  :   Evaluating MQ in secret map\n\n");


	uint8_t s1[_PUB_N_BYTE];
	uint8_t d1[_PUB_M_BYTE],d2[_PUB_M_BYTE];
	uint8_t tt[_PUB_N_BYTE];

	printf("\n\n============= Generating MQ polynomials by 'rainbow_genkey()' function ==============\n");

	uint8_t *sk = (uint8_t*) malloc( _SEC_KEY_LEN );
	uint8_t * qp_pk = (uint8_t*) malloc( _PUB_KEY_LEN );
	rainbow_genkey(qp_pk,sk);

	printf("\n\n============= key generated. test start:  ==============\n\n");

	gf256v_rand(s1,_PUB_N_BYTE);

	mpkc_pub_map_gf256( d1 , qp_pk , s1 );
	mpkc_pub_map_gf256_n_m( d2 , qp_pk , s1 , _PUB_N , _PUB_M );

	printf("[inp: ] "); gf256v_fdump( stdout , s1 , _PUB_N_BYTE ); printf("\n");
	printf("[out1:] "); gf256v_fdump( stdout , d1 , _PUB_M_BYTE ); printf("\n");
	printf("[out2:] "); gf256v_fdump( stdout , d2 , _PUB_M_BYTE ); printf("\n\n");

	unsigned pass = 0;

	for(unsigned i=0;i<TEST_RUN;i++) {
		gf256v_rand(s1,_PUB_N_BYTE);

		mpkc_pub_map_gf256( d1 , qp_pk , s1 );
		mpkc_pub_map_gf256_n_m( d2 , qp_pk , s1 , _PUB_N , _PUB_M );

		memcpy( tt , d2 , _PUB_M_BYTE );
		gf256v_add( tt , d1 , _PUB_M_BYTE );

		if( ! gf256v_is_zero(tt,_PUB_M_BYTE) ) {
			printf("fail:[%d]\n",i);
			gf256v_fdump( stdout , s1 , _PUB_N_BYTE ); printf("\n");
			gf256v_fdump( stdout , d1 , _PUB_M_BYTE ); printf("\n");
			gf256v_fdump( stdout , d2 , _PUB_M_BYTE ); printf("\n\n");
			return -1;
		} else pass++;
	}
	printf("%d/%d test success.\n\n", pass , TEST_RUN );

	free( sk );
	free( qp_pk );

	return 0;
}

