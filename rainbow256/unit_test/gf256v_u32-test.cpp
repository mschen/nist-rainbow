
#include <stdio.h>

#include "ut_gf256.h"

#include "ut_gf256v_u32.h"





int main()
{


	printf("\n\n");
	printf("  Tester for arithmetics in GF(256) u32 vectors.\n\n");
	printf("  Presuming the correctness of ut_gf256_mul() \n\n" );
	printf("  ut_gf256v_mul_u32()      :   Multiplication in GF(256)\n" );
	printf("  ut_gf256v_squ_u32()      :   Square in GF(256)\n" );



	printf("\n\n============= Tester for ut_gf256v_mul_u32() function ==============\n");

	for(unsigned i=0;i<256;i++) {
		for(unsigned j=0;j<256;j++) {
			uint32_t ii = i;
			for(unsigned k=0;k<3;k++) { ii<<=8; ii|=i; }

			uint32_t c = ut_gf256v_mul_u32(ii,j);
			unsigned char d = ut_gf256_mul( i , j);

			for(unsigned k = 0 ;k < 4;k++) {
				unsigned char e = c&0xff;
				c >>= 8;

				if( e != d ) {
					printf("%x x %x:\n",i,j);
					printf("expect: %x\n", d );
					printf("got (%d) : %x\n", k , e );

					goto L_FAIL;
				}
			}

		}
	}

	printf("\nPASS.\n");

	printf("\n\n============= Tester for ut_gf256v_squ_u32() function ==============\n");

	for(unsigned i=0;i<256;i++) {
		uint32_t ii = i;
		for(unsigned k=0;k<3;k++) { ii<<=8; ii|=i; }

		uint32_t c = ut_gf256v_squ_u32(ii);
		unsigned char d = ut_gf256_mul( i , i);

		for(unsigned k = 0 ;k < 4;k++) {
			unsigned char e = c&0xff;
			c >>= 8;

			if( e != d ) {
				printf("%x^2:\n",i);
				printf("expect: %x\n", d );
				printf("got (%d) : %x\n", k , e );
				goto L_FAIL;
			}
		}
	}

	printf("\nPASS.\n\n");

	return 0;

L_FAIL:
	printf("\nFIAL.\n");
	return -1;
}

