# rainbow

Rainbow GF(256) 40, 24, 24

This is a reference implementation.

This implementation aims for introducing the algorithm, instead of efficiency.


#usage:

0. (Optional) Edit core/rainbow_config.h for setting (V1,O1,O2).
   The default: (40,24,24). Tested: (40,16,32) (40,24,24)

1. Make for 3 executables: rainbow-genkey, rainbow-sign, and rainbow-verify .

2. (generate key pairs)
  rainbow-genkey  pk_file_name  sk_file_name [selfdefined_random_file]

3. (sign a file)
  rainbow-sign  sk_file_name file_name_to_be_signed [selfdefined_random_file]

   (Or redirect the signature to an output file)
  rainbow-sign  sk_file_name file_name_to_be_signed [selfdefined_random_file] | tee signature_file_name


4. (verify a signature)
  rainbow-verify  pk_file_name  signature_file_name  file_name_to_be_signed



# (Optional) Use self-defined randomness:

1. Prepare a binary file and input the file name when executing the command line tools.
   The contents of the file are the ``randomness'' used.

2. The used randomness will be outputted.
