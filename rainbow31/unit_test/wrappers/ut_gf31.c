
#include "ut_gf31.h"



#include "gf31.h"
/// where the real code in.





unsigned char ut_gf31_is_nonzero( unsigned char a ) {
	return gf31_is_nonzero( a );
}


unsigned char ut_gf31_add( unsigned char a , unsigned char b ) {
	return gf31_add( a , b );
}

unsigned char ut_gf31_sub( unsigned char a , unsigned char b ) {
	return gf31_sub( a , b );
}



unsigned char ut_gf31_mul( unsigned char a , unsigned char b ) {
	return gf31_mul( a , b );
}

unsigned char ut_gf31_squ( unsigned char a ) {
	return gf31_squ( a );
}

unsigned char ut_gf31_inv( unsigned char a ) {
	return gf31_inv( a );
}

