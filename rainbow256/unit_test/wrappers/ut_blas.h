#ifndef _UT_BLAS_H_
#define _UT_BLAS_H_


#include "stdint.h"


#ifdef  __cplusplus
extern  "C" {
#endif



void ut_gf256v_add( uint8_t * accu_b, const uint8_t * a , unsigned _num_byte );


void ut_gf16v_mul_scalar( uint8_t * a, uint8_t b , unsigned _num_byte );

void ut_gf256v_mul_scalar( uint8_t *a, uint8_t b, unsigned _num_byte );


void ut_gf16v_madd( uint8_t * accu_c, const uint8_t * a , uint8_t b, unsigned _num_byte );

void ut_gf256v_madd( uint8_t * accu_c, const uint8_t * a , uint8_t b, unsigned _num_byte );





#ifdef  __cplusplus
}
#endif

#endif
