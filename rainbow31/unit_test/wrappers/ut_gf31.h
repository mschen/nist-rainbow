#ifndef _UT_GF31_H_
#define _UT_GF31_H_



#ifdef  __cplusplus
extern  "C" {
#endif


unsigned char ut_gf31_is_nonzero( unsigned char a );


unsigned char ut_gf31_add( unsigned char a , unsigned char b );

unsigned char ut_gf31_sub( unsigned char a , unsigned char b );



unsigned char ut_gf31_mul( unsigned char a , unsigned char b );

unsigned char ut_gf31_squ( unsigned char a );

unsigned char ut_gf31_inv( unsigned char a );





#ifdef  __cplusplus
}
#endif

#endif
