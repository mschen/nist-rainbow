
run_benchmark()
{
  make clean; make PARAM=1 PROJ_DIR=$1
  ./rainbow-benchmark
  OUT=$?
  if [ "$OUT"  == "0" ]; then
    echo "==============  PARAM 1 PASS =================="
  else
    echo "FAIL."
    return -1
  fi
  make clean; make PARAM=2 PROJ_DIR=$1
  ./rainbow-benchmark
  OUT=$?
  if [ "$OUT"  == "0" ]; then
    echo "==============  PARAM 2 PASS =================="
  else
    echo "FAIL."
    return -1
  fi
  make clean; make PARAM=3 PROJ_DIR=$1
  ./rainbow-benchmark
  OUT=$?
  if [ "$OUT"  == "0" ]; then
    echo "==============  PARAM 3 PASS =================="
  else
    echo "FAIL."
    return -1
  fi
  return 0
}


### Main script start ###

echo $(git log -n 1)

echo "\$# = " $#
DIR=avx2

if [ "$#" == "1" ]; then
  DIR=$1
fi


echo " ================ BENCHMARK DIR: $DIR ================"

run_benchmark $DIR
