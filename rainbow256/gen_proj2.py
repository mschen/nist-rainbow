#!/usr/bin/python

import os, errno, shutil
from shutil import copyfile


proj_name = [ 'Ic' , 'IIIc' , 'Vc' ]

config_file_name = 'rainbow_config.h'

more_define = [ '#define _RAINBOW_256_40_20_20_\n' , '#define _RAINBOW_256_68_36_36_\n' , '#define _RAINBOW_256_92_48_48_\n' ]

path_def = '../sub'
path_ref = path_def + '/Reference_Implementation'
path_opt = path_def + '/Optimized_Implementation/amd64'
path_alt = path_def + '/Optimized_Implementation/avx2'


def find_1st_match_line( file_name , phrase ):
  try:
    content = list( open( file_name ) )
  except :
    raise
  for i in range( len(content) ) :
    if content[i].startswith( phrase ) :
      return i
  return -1

def file_change_str( file_name , line_idx , comment ):
  try:
    content = list( open( file_name ) )
  except :
    pass
  if len( content ) <= line_idx :
    return
  try:
    fp = open( file_name , "w" )
  except :
    return
  for i in range(len(content)):
    if line_idx == i :
      fp.write( comment )
    else:
      fp.write( content[i] )
  fp.close()

def copy_files( dest , src ):
    src_files = os.listdir(src)
    for file_name in src_files:
        full_file_name = os.path.join(src, file_name)
        if (os.path.isfile(full_file_name)):
            shutil.copy2(full_file_name, dest)

def gen_proj_dirs( p1 ):
    try:
        for i in proj_name :
          p2 = p1 + '/' + i
          print 'generate: ' + p2 + '\n'
          os.makedirs(p2)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

def generate_directories() :
  try:
    os.makedirs( path_def + '/Supporting_Documentation')
  except OSError as e:
    print 'gen dir fail.\n'
    if e.errno != errno.EEXIST:
      raise
  try:
    os.makedirs( path_ref )
  except OSError as e:
    print 'gen dir fail.\n'
    if e.errno != errno.EEXIST:
      raise
  try:
    gen_proj_dirs( path_ref )
  except OSError as e:
    print 'gen dir fail.\n'
    if e.errno != errno.EEXIST:
      raise
  try:
    os.makedirs( path_opt )
  except OSError as e:
    print 'gen dir fail.\n'
    if e.errno != errno.EEXIST:
      raise
  try:
    gen_proj_dirs( path_opt )
    os.makedirs( path_alt )
  except OSError as e:
    print 'gen dir fail.\n'
    if e.errno != errno.EEXIST:
      raise
  try:
    gen_proj_dirs( path_alt )
  except OSError as e:
    print 'gen dir fail.\n'
    if e.errno != errno.EEXIST:
      raise

generate_directories()

src_common = './common/'
src_ref = './ref/'
src_amd64 = './amd64/'
src_avx2 = './avx2/'


for i in range( len(proj_name) ):
  dest_ref = path_ref + '/' + proj_name[i] + '/'
  dest_amd64 = path_opt + '/' + proj_name[i] + '/'
  dest_avx2 = path_alt + '/' + proj_name[i] + '/'

  copy_files( dest_ref , src_ref )
  copy_files( dest_ref , src_common )
  line_cnt = find_1st_match_line( dest_ref + config_file_name , '#define _RAINBOW' )
  file_change_str( dest_ref + config_file_name , line_cnt , more_define[i] )
  print "find match: l:" + str(line_cnt) + " -> " + more_define[i] + '\n'

  copy_files( dest_amd64 , src_amd64 )
  copy_files( dest_amd64 , src_common )
  line_cnt = find_1st_match_line( dest_amd64 + config_file_name , '#define _RAINBOW' )
  file_change_str( dest_amd64 + config_file_name , line_cnt , more_define[i] )
  print "find match: l:" + str(line_cnt) + " -> " + more_define[i] + '\n'

  copy_files( dest_avx2 , src_avx2 )
  copy_files( dest_avx2 , src_common )
  line_cnt = find_1st_match_line( dest_avx2 + config_file_name , '#define _RAINBOW' )
  file_change_str( dest_avx2 + config_file_name , line_cnt , more_define[i] )
  print "find match: l:" + str(line_cnt) + " -> " + more_define[i] + '\n'


shutil.copy2( './PQCgenKAT_sign.c' , path_ref)
shutil.copy2( './rainbow-genkey.c' , path_ref)
shutil.copy2( './rainbow-sign.c' , path_ref)
shutil.copy2( './rainbow-verify.c' , path_ref)
shutil.copy2( './utils.c' , path_ref)
shutil.copy2( './utils.h' , path_ref)
shutil.copy2( './Makefile.KAT' , path_ref+'/Makefile')

shutil.copy2( './PQCgenKAT_sign.c' , path_opt)
shutil.copy2( './rainbow-genkey.c' , path_opt)
shutil.copy2( './rainbow-sign.c' , path_opt)
shutil.copy2( './rainbow-verify.c' , path_opt)
shutil.copy2( './utils.c' , path_opt)
shutil.copy2( './utils.h' , path_opt)
shutil.copy2( './Makefile.KAT' , path_opt+'/Makefile')

shutil.copy2( './PQCgenKAT_sign.c' , path_alt)
shutil.copy2( './rainbow-genkey.c' , path_alt)
shutil.copy2( './rainbow-sign.c' , path_alt)
shutil.copy2( './rainbow-verify.c' , path_alt)
shutil.copy2( './utils.c' , path_alt)
shutil.copy2( './utils.h' , path_alt)
shutil.copy2( './Makefile.KAT.avx2' , path_alt+'/Makefile')
