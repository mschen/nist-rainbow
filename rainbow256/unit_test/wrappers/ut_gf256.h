#ifndef _UT_GF256_H_
#define _UT_GF256_H_



#ifdef  __cplusplus
extern  "C" {
#endif



unsigned char ut_gf256_mul( unsigned char a , unsigned char b );

unsigned char ut_gf256_squ( unsigned char a );

unsigned char ut_gf256_inv( unsigned char a );

unsigned char ut_gf256_is_nonzero( unsigned char a );



unsigned char ut_gf256_mul_gf16( unsigned char a , unsigned char gf16_b );


#ifdef  __cplusplus
}
#endif

#endif
