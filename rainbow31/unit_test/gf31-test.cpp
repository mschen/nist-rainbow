
#include <stdio.h>

#include "ut_gf31.h"




int main()
{


	printf("\n\n");
	printf("  Tester for arithmetics in GF(31).\n\n");

	printf("  ut_gf31_is_nonzero() :   is an element in GF(31) is 0 ?\n" );

	printf("  ut_gf31_add()        :   Addition     in GF(31)\n" );
	printf("  ut_gf31_sub()        :   Substraction in GF(31)\n" );

	printf("  ut_gf31_mul()        :   Multiplication in GF(31)\n" );
	printf("  ut_gf31_squ()        :   Square in GF(31)\n" );
	printf("  ut_gf31_inv()        :   Multiplicativ Inverse in GF(31)\n" );



	printf("\n\n============= Tester for ut_gf31_is_nonzero() function ==============\n");

	for(unsigned i=0;i<31;i++) {
		unsigned char c = ut_gf31_is_nonzero(i);

		if( 0 == i ) {
			if( 1 == c ) {
				printf("test(0) --> %d.\n", c );
				goto L_FAIL;
			}
			continue;
		}

		if( 1 != c ) {
			printf("test(%x) -> %x\n",i , c );

			goto L_FAIL;
		}
	}

	printf("PASS.\n");

	printf("\n\n============= Tester for ut_gf31_add() function ==============\n");

	for(unsigned i=0;i<31;i++) {
		for(unsigned j=0;j<31;j++) {
			unsigned char c = ut_gf31_add(i,j);

			unsigned d = (i+j)%31;

			if( d != c ) {
				printf("%d + %d:\n" ,i,j );
				printf("expect: %d\n" , d );
				printf("got   : %d\n" , c );

				goto L_FAIL;
			}

		}
	}

	printf("PASS\n");


	printf("\n\n============= Tester for ut_gf31_sub() function ==============\n");

	for(unsigned i=0;i<31;i++) {
		for(unsigned j=0;j<31;j++) {
			unsigned char c = ut_gf31_sub(i,j);

			unsigned d = (31+i-j)%31;

			if( d != c ) {
				printf("%d x %d:\n" ,i,j );
				printf("expect: %d\n" , d );
				printf("got   : %d\n" , c );

				goto L_FAIL;
			}

		}
	}

	printf("PASS\n");


	printf("\n\n============= Tester for ut_gf31_mul() function ==============\n");

	for(unsigned i=0;i<31;i++) {
		for(unsigned j=0;j<31;j++) {
			unsigned char c = ut_gf31_mul(i,j);

			unsigned d = (i*j)%31;

			if( d != c ) {
				printf("%d x %d:\n" ,i,j );
				printf("expect: %d\n" , d );
				printf("got   : %d\n" , c );

				goto L_FAIL;
			}

		}
	}

	printf("PASS\n");

	printf("\n\n============= Tester for ut_gf31_squ() function ==============\n");

	for(unsigned i=0;i<31;i++) {
		unsigned char c = ut_gf31_squ(i);
		unsigned char e = ut_gf31_mul(i,i);
		if( e != c ) {
			printf("squ(%x):\n",i);
			printf("expect: %x\n", e );
			printf("got   : %x\n", c );

			goto L_FAIL;
		}

	}

	printf("PASS.\n");

	printf("\n\n============= Tester for ut_gf31_inv() function ==============\n");

	for(unsigned i=0;i<31;i++) {
		unsigned char b = ut_gf31_inv(i);
		unsigned char c = ut_gf31_mul(i,b);

		if( 0 == i ) continue;

		if( 1 != c ) {
			printf("inv(%x) -> %x\n",i , b );
			printf("%x x %x -> %x != 1\n",i , b , c );

			goto L_FAIL;
		}
	}

	printf("PASS.\n");


	return 0;

L_FAIL:
	printf("\nFIAL.\n");
	return -1;
}

