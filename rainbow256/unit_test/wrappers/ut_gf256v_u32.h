#ifndef _UT_GF256V_U32_H_
#define _UT_GF256V_U32_H_


#include "stdint.h"


#ifdef  __cplusplus
extern  "C" {
#endif



uint32_t ut_gf256v_mul_u32( uint32_t a , unsigned char b );

uint32_t ut_gf256v_squ_u32( uint32_t a );




#ifdef  __cplusplus
}
#endif

#endif
