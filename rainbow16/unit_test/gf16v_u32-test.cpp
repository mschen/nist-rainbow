
#include <stdio.h>

#include "ut_gf16.h"

#include "ut_gf16v_u32.h"





int main()
{


	printf("\n\n");
	printf("  Tester for arithmetics in GF(16) u32 vectors.\n\n");
	printf("  Presuming the correctness of ut_gf16_mul() \n\n" );
	printf("  ut_gf16v_mul_u32()      :   Multiplication in GF(16)\n" );
	printf("  ut_gf16v_squ_u32()      :   Square in GF(16)\n" );
	printf("  ut_gf16v_mul_8_u32()    :   Multiplication by 0x8 in GF(16)\n" );



	printf("\n\n============= Tester for ut_gf16v_mul_u32() function ==============\n");

	for(unsigned i=0;i<16;i++) {
		for(unsigned j=0;j<16;j++) {
			uint32_t ii = i;
			for(unsigned k=0;k<7;k++) { ii<<=4; ii|=i; }

			uint32_t c = ut_gf16v_mul_u32(ii,j);
			unsigned char d = ut_gf16_mul( i , j);

			for(unsigned k = 0 ;k < 8;k++) {
				unsigned char e = c&0xf;
				c >>= 4;

				if( e != d ) {
					printf("%x x %x:\n",i,j);
					printf("expect: %x\n", d );
					printf("got (%d) : %x\n", k , e );

					goto L_FAIL;
				}
			}

		}
	}

	printf("\nPASS.\n");

	printf("\n\n============= Tester for ut_gf16v_squ_u32() function ==============\n");

	for(unsigned i=0;i<16;i++) {
		uint32_t ii = i;
		for(unsigned k=0;k<7;k++) { ii<<=4; ii|=i; }

		uint32_t c = ut_gf16v_squ_u32(ii);
		unsigned char d = ut_gf16_mul( i , i);

		for(unsigned k = 0 ;k < 8;k++) {
			unsigned char e = c&0xf;
			c >>= 4;

			if( e != d ) {
				printf("%x^2:\n",i);
				printf("expect: %x\n", d );
				printf("got (%d) : %x\n", k , e );
				goto L_FAIL;
			}
		}
	}

	printf("\nPASS.\n");


	printf("\n\n============= Tester for ut_gf16_mul_8() function ==============\n");

	for(unsigned i=0;i<16;i++) {
		uint32_t ii = i;
		for(unsigned k=0;k<7;k++) { ii<<=4; ii|=i; }

		uint32_t c = ut_gf16v_mul_8_u32(ii);
		unsigned char d = ut_gf16_mul( i , 8 );

		for(unsigned k = 0 ;k < 8;k++) {
			unsigned char e = c&0xf;
			c >>= 4;

			if( e != d ) {
				printf("%x x 8:\n",i);
				printf("expect: %x\n", d );
				printf("got (%d) : %x\n", k , e );
				goto L_FAIL;
			}
		}
	}

	printf("\nPASS.\n");

	return 0;

L_FAIL:
	printf("\nFIAL.\n");
	return -1;
}

