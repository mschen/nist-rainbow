
#include <stdio.h>

#include "rainbow_config.h"

#include "blas.h"

#include "string.h"  // for memcpy
#include "prng_utils.h"

static
void transpose_l2( uint8_t * r , const uint8_t * a )
{
        for(unsigned i=0;i<_O2;i++) {
                for(unsigned j=0;j<_O2;j++) {
                        r[i*_O2+j] = a[j*_O2+i];
                }
        }
}


static
unsigned linear_solver_l2( uint8_t * r , const uint8_t * mat_inp , const uint8_t * cc )
{
        uint8_t mat[_O2*(_O2+1)] ;

	/// copy input matrix to temporary matrix
        for(unsigned i=0;i<_O2;i++) {
                memcpy( mat+i*(_O2+1) , mat_inp+i*_O2 , _O2 );
                mat[i*(_O2+1) + _O2] = cc[i];
        }

        unsigned rv = gf256mat_gauss_elim( mat , _O2 , _O2 + 1 );
        gf256mat_submat( r , 1 , _O2 , mat , _O2 + 1 , _O2 );
        return rv;
}


int main( int argc , char **argv )
{

	prng_seed_file( "/dev/random" );


	printf("\n===============================================================\n\n");
	printf("  Tester for Gauss elimination.\n\n");
	printf("  function:                gf256mat_gauss_elim()     in blas.h\n" );
	printf("  matrix size: row major:  %d x %d\n\n", _O2, _O2 );
	printf("\n===============================================================\n\n");

	uint8_t mat_row[_O2*_O2];
	uint8_t mat_col[_O2*_O2];

	uint8_t ref[_O2];
	uint8_t constant_terms[_O2];
	uint8_t sol[_O2];

	/// generate row-major matrix
	gf256v_rand( mat_row , _O2*_O2 );
	/// transpose matrix (for multiplication)
	transpose_l2( mat_col , mat_row );

	/// generate test input
	gf256v_rand( ref , _O2 );
	gf256mat_prod( constant_terms , mat_col , _O2 , _O2 , ref ); ///  compute: constant_terms <-- mat_col x ref

	unsigned r = linear_solver_l2( sol , mat_row , constant_terms );


	printf("solved? %d\n", r );
	printf("inp: "); gf256v_fdump( stdout , ref , _O2 ); printf("\n");
	printf("sol: "); gf256v_fdump( stdout , sol , _O2 ); printf("\n");

	gf256v_add( sol , ref , _O2 );
	printf("\nTEST: ");
	if( 0 == r ) printf("?\n\n");
	else {
		if( gf256v_is_zero( sol , _O2 ) ) printf("passed\n\n");
		else printf("FAIL.\n\n");
	}

	return 0;
}

