
#include <stdio.h>

#include "ut_gf16.h"
#include "ut_gf256.h"


/// presuming the correctness of ut_gf16_mul()
static unsigned char _ck_gf256_mul( unsigned char i , unsigned char j )
{
	unsigned char i0 = i&0xf;
	unsigned char i1 = (i>>4)&0xf;
	unsigned char j0 = j&0xf;
	unsigned char j1 = (j>>4)&0xf;

	unsigned char k0 = ut_gf16_mul( i0 , j0 );
	unsigned char k1 = ut_gf16_mul( i1 , j0 ) ^ ut_gf16_mul( i0 , j1 );
	unsigned char k2 = ut_gf16_mul( i1 , j1 );
	/// reduce
	k1 ^= k2;
	k0 ^= ut_gf16_mul( k2 , 0x8 );
	return (k1<<4)^k0;
}



int main()
{


	printf("\n\n");
	printf("  Tester for arithmetics in GF(256).\n\n");
	printf("  ut_gf256_mul()        :   Multiplication in GF(256)\n" );
	printf("  ut_gf256_squ()        :   Square in GF(256)\n" );
	printf("  ut_gf256_inv()        :   Multiplicativ Inverse in GF(256)\n" );
	printf("  ut_gf256_is_nonzero() :   is a byte is 0 ?\n" );
	printf(" ------------------------  \n");
	printf("  ut_gf256_mul_gf16()   :   Multiplication by an element in GF(16)\n" );


	printf("\n\n============= Tester for ut_gf256_mul() function ==============\n");

	for(unsigned i=0;i<256;i++) {
		for(unsigned j=0;j<256;j++) {
			unsigned char c = ut_gf256_mul(i,j);

			unsigned char d = _ck_gf256_mul( i , j );


			if( c != d ) {
				printf("%x x %x:\n",i,j);
				printf("expect: %x\n", d );
				printf("got   : %x\n", c );

				goto L_FAIL;
			}

		}
	}

	printf("PASS.\n");

	printf("\n\n============= Tester for ut_gf256_squ() function ==============\n");

	for(unsigned i=0;i<256;i++) {
		unsigned char c = ut_gf256_squ(i);
		unsigned char e = ut_gf256_mul(i,i);
		if( e != c ) {
			printf("squ(%x):\n",i);
			printf("expect: %x\n", e );
			printf("got   : %x\n", c );

			goto L_FAIL;
		}

	}

	printf("PASS.\n");

	printf("\n\n============= Tester for ut_gf256_inv() function ==============\n");

	for(unsigned i=0;i<256;i++) {
		unsigned char b = ut_gf256_inv(i);
		unsigned char c = ut_gf256_mul(i,b);

		if( 0 == i ) continue;

		if( 1 != c ) {
			printf("inv(%x) -> %x\n",i , b );
			printf("%x x %x -> %x != 1\n",i , b , c );

			goto L_FAIL;
		}
	}

	printf("PASS.\n");


	printf("\n\n============= Tester for ut_gf256_is_nonzero() function ==============\n");

	for(unsigned i=0;i<256;i++) {
		unsigned char c = ut_gf256_is_nonzero(i);

		if( 0 == i ) {
			if( 1 == c ) {
				printf("test(0) --> %d.\n", c );
				goto L_FAIL;
			}
			continue;
		}

		if( 1 != c ) {
			printf("test(%x) -> %x\n",i , c );

			goto L_FAIL;
		}
	}

	printf("PASS.\n");


	printf("\n\n============= Tester for ut_gf256_mul_gf16() function ==============\n");

	for(unsigned i=0;i<256;i++) {
		for(unsigned j=0;j<16;j++) {
			unsigned char c = ut_gf256_mul_gf16(i,j);
			unsigned char e = ut_gf256_mul( i , j );
			if( e != c ) {
				printf("test %x x %x:\n",i,j);
				printf("expect: %x\n", e );
				printf("got   : %x\n", c );

				goto L_FAIL;
			}
		}

	}

	printf("PASS.\n");



	return 0;

L_FAIL:
	printf("\nFIAL.\n");
	return -1;
}

